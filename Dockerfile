#BOF

# original MAINTAINER Brian Bassett <bbassett1276@gmail.com>
# FROM bbassett/openscad

FROM debian:stable-slim

RUN apt-get update && apt-get install -y --no-install-recommends \
	ca-certificates \
	git \
	make \
	openscad \
    && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /root/.local/share /.local/share

RUN apt-get update && apt-get install --assume-yes git-restore-mtime bsdmainutils xvfb jq curl rsync

RUN [ -d /local ] || mkdir -pv /local

WORKDIR /local
RUN curl -O https://files.openscad.org/openscad-2021.01.src.tar.gz

RUN zcat openscad-2021.01.src.tar.gz | tar xvf -

WORKDIR /local/openscad-2021.01

RUN bash ./scripts/uni-get-dependencies.sh

RUN bash ./scripts/check-dependencies.sh 

RUN qmake openscad.pro

RUN make

RUN /usr/bin/openscad --version

RUN install ./openscad /usr/bin/openscad

RUN which openscad

RUN ./openscad --version

RUN /usr/bin/openscad --version

#EOF
